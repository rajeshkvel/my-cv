<a href="https://gitmoji.carloscuesta.me">
  <img src="https://img.shields.io/badge/gitmoji-%20😜%20😍-FFDD67.svg?style=flat-square" alt="Gitmoji">
</a>

# :page_facing_up: Welcome to my online CV repo

#### > See it online [here](https://cv.johannchopin.fr/)


----
### npm CLI:
* `npm run dev` : build project in `/build/dev` folder and open it on server `http://localhost:1234` (**Warning**: all links are absolute)
* `npm run devLocal` : build project with relative links in `/build/devLocal` folder
* `npm run prod` : build project with relative links and minify code in `/build/prod/{appVersion}` folder
